---
title: talks
date: 2017-01-29 17:30:41
---
In 2015 I started giving talks about Docker and Kubernetes. It was really hard the first time, but it gets better on every talk.
 I did not regret to leave my comfort zone. Thanks to my colleagues for encouring me to do this! 
 
 
In the meanwhile I was speaking at:

2015:
- "Luminis DevCon" (https://devcon.luminis.eu/):  
    Session with my colleague Jan-Willem Janssen: "Kubernetes: taking Docker to the next level"  
    Video: https://www.youtube.com/watch?v=ymits1rv4g4

- JUG Dortmund [GER]  
    Short talk "Orchestrierung von Docker Containern in einem Cluster mit Kubernetes"  
    Info: http://www.jugdo.de/2015/08/15/jug-dortmund-sommerfest/

- Devoxx Belgium  
    Quicki "Managing Docker containers in a cluster - introducing Kubernetes"  
    Slides: http://de.slideshare.net/MarcSluiter1/managing-docker-containers-in-a-cluster-introducing-kubernetes  
    Video: https://www.youtube.com/watch?v=2G-A6Knjrvo
    

2016:

- JUG Münster [GER]  
    Talk "Docker Orchestrierung mit Kubernetes"  
    Info: http://www.jug-muenster.de/vortrag-docker-orchestrierung-mit-kubernetes-1439/

- Javaland  
    Talk "Docker Orchestration in a Cluster - Introducing Kubernetes"  
    Info: https://www.doag.org/konferenz/konferenzplaner/konferenzplaner_details.php?locS=0&id=499959&vid=509429
    
- Devoxx Belgium  
    Quickie "15 Kubernetes features in 15 minutes"  
    Slides: http://de.slideshare.net/MarcSluiter1/marc-sluiter-15-kubernetes-features-in-15-minutes  
    Video: https://www.youtube.com/watch?v=o85VR90RGNQ
    
2019:

- RivieraDev  
    Talk "Making virtual machines cloud native with KubeVirt"  
    Info: https://www.rivieradev.fr/session/683  
    Slides & Demo: https://docs.google.com/presentation/d/1fOpizP4-_O_09Vs8UKfQwECG07FhM_mrMdFUSryORkk  
    

