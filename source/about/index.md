---
title: about
date: 2016-01-28 15:57:01
---
{% img http://www.gravatar.com/avatar/18d85d349dceb3f28397e1938f3a7309?s=200 200 "That is me... at least if I uploaded an image of me to gravatar ;)" %}


I'm Marc Sluiter alias @slintes. I live in Germany and work for Red Hat.

I started programming professionally around 2001. Since then I worked for
many customers with (at least often) interesting technologies. After many years with mainly Java, recently my main interest moved to software engineering in Go and in the cloud native ecosystem.

At my former employer Luminis Technologies I was busy with mainly 3 projects:

- ["Amdatu"](http://www.amdatu.org) is an open source project, which provides components for modular software development based on OSGi.
- ["INAETICS"](http://www.inaetics.org) is another open source project about developing and deploying distributed applications. Here I had my first contact
 with Docker and Kubernetes.
- ["Cloud RTI"](https://www.luminis.eu/what-we-do/technology-solutions/cloud-rti/) is our commercial offering for hosting your applications in a modern fashion, based on Docker and Kubernetes.

In May 2018 I started working for Red Hat on the [kubevirt](https://www.kubevirt.io/) project.

In 2015 my colleagues at Luminis encouraged me to start giving talks at conferences. See more about this [at its dedicated page](../talks).

In my private life I'm administrating my own (old fashioned) dedicated server (currently moving to new cloud servers) in a data center in Germany.
It runs my own mail server and hosts some websites. Did I say private life and administrating a server in one sentence? Well, it's fun, at least most times ;)

Besides that I own [2 cats](https://goo.gl/photos/EcwEdYppL7UXGg8i7) and a fish tank.

While I used to play the guitar myself some time ago, I'm almost only a concert-goer in the meanwhile. I like listening to Blues and Bluesrock.

That's it, if you have any questions, don't hesitate to contact me via [mail](mailto:marc@slintes.net) or on [twitter](https://www.twitter.com/slintes).
