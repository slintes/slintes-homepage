---
title: Hello World!
date: 2016-01-28 14:23:05
tags:
- Private
---
What should I write? Like every good software engineer I'll start with:

{% blockquote every software engineer on earth ;) %}
Hello World!
{% endblockquote %}

During the last months I sometimes thought: mh, that was interesting, and might be useful for others too.
So here we go, today I finally decided to start a personal blog.

What can you expect here?

- Most posts will probably be related to software engineering, especially about Java and modular development with OSGi.
- Another big topic will be Docker and Kubernetes, because I'm involved in projects where we use these technologies for deploying our and your applications.
- Sometimes I find some nice tips and tricks, which at least I didn't know yet, which make daily life easier on OSX. Some of them are probably worth sharing here, too.
- And last but not least you might find some private topics here as well... not sure about that yet. But if so, that might include some cat content. You have been warned ;)

If you want to know more about me, also check out the [About](/about) page.

That's enough for a hello world post. Enjoy your day!

{% img /images/helloWorld.jpg 300 Hello World %}
