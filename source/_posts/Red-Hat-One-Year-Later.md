---
title: Red Hat - One year later
date: 2019-05-28 13:17:19
tags:
    - KubeVirt
---
Around one year ago I was happy to announce that I joined Red Hat and the KubeVirt project. What happened since then?

## Red Hat

Well, what should I say, I can only confirm what I heard from several Red Hatters before I joined: I found a very remote
friendly company with a great culture, and helpful managers and team mates. Compared to my previous companies Red Hat is huge, but most times
it doesn't feel like that. For sure you heard that Red Hat is very probably going to be even more huge soonish, as part of
IBM. That were really "interesting" news a few months ago, but in the meanwhile I'm optimistic that Red Hat will keep its
culture.

Btw: we (Red Hat in general, but also KubeVirt) are still hiring, ping me if you find an interesting position!  
https://www.redhat.com/en/jobs 

## KubeVirt

I have to admit, when I first read about KubeVirt, I was like: WTH? Why should I run VMs in a Kubernetes cluster, that is
a step backwards. But it turns out that there are indeed usecases. Most important usecase is: when you need to keep virtual
machines for whatever reasons (lack of resources for migration to containers, need for specific kernel features, ...), but
also already have containerized workloads, KubeVirt allows you to run both on the same cluster, sharing network and storage,
and using only one orchestration tool, Kubernetes / Openshift. Also, there a community projects, which use KubeVirt as a
Kubernetes cloud provider, for running... Kubernetes on VMs on Kubernetes. Very interesting!

Technically, to me getting familiar with KubeVirt felt quite challenging. My Go experience was a bit limited, and the project
is quite huge, and has some kind of complicated parts. But at the end this is exactly what I was searching for. And according
to the feedback of my colleagues, it seems that I'm making good progress, even though it still feels different to me sometimes.
But I really enjoy learning more and more especially about custom resources, and how to implement robust controllers for them.

## Conferences

### RivieraDev

Near the end of my first year I finally felt confident enough for submitting at least introductory talks about KubeVirt
to conferences. And so, in May I had my first session, "Making virtual machines cloud native with KubeVirt" [0], at the
famous and great RivieraDev conference in southern France. That was exciting but also fun, it was great to see that there
actually is some serious interest in KubeVirt! :)

{% img /images/rivieradev19.jpg 400 RivieraDev %}


### KubeCon EU

Directly after RivieraDev, some team mates and me were allowed to visit KubeCon EU in Barcelona, the place where my Red Hat adventure
started a year ago. Amazing and crazy how this conference is growing, and how many companies are jumping on the cloud native
train.

{% img /images/kubecon19.jpg 400 KubeCon %}

That's it, thanks for reading! Let's see if I need another year for the next post ;)

[0] https://docs.google.com/presentation/d/1fOpizP4-_O_09Vs8UKfQwECG07FhM_mrMdFUSryORkk
 

