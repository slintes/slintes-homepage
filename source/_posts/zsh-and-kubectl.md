---
title: zsh and kubectl
date: 2016-04-20 10:58:39
tags:
- Kubernetes
- Zsh
  
---
Oh-my-zsh
=========

Yesterday I finally installed [oh-my-zsh](http://ohmyz.sh/). It was on my to-do list for longer time already,
because I read so many good things about it... but having things on a todo list and finally taking the time for doing them is 
a big difference sometimes ;)

However, the installation went fine, and after that you can, or better to say have to spend some time on configuration,
which basically means chosing a theme and the plugins you want to use.

A good thing about the installation is, that it took over the `$PATH` variable from the old shell. But unfortunately 
you now have one big `export PATH=...` line, while you probably had a more structered version before. At least I had.
So I deleted that line from the config, and copied my old PATH config from the now unused bash configuration.

kubectl command completion
==========================

A few days ago I came across a small [blog post](https://blog.fabric8.io/enable-bash-completion-for-kubernetes-with-kubectl-506bc89fe79e#.v5w2d8q5q)
about `kubectl` (the Kubernetes command line tool) command completion. That is really useful, so wanted to have that
for zsh too, and I found this:
 
{% blockquote @the1stein &nbsp; https://twitter.com/the1stein/status/711691459301351424 %}
kubectl completion finally also for zsh https://github.com/kubernetes/kubernetes/pull/23262
{% endblockquote %}
 
I  didn't find instructions on how to add that to the zsh config, and it took me some minutes to find out, so I'd like to share it here:

1. clone kubernetes master (the zsh completion is not in a release yet): `git clone https://github.com/kubernetes/kubernetes.git`
2. add this to `~/.zshrc`: `source $HOME/path/to/kubernetes/contrib/completions/zsh/kubectl`

<del>I got an error at the first try, and it turned out that `gnu-sed` was the problem, which I had installed with `brew`.
After uninstalling it, it worked fine:</del> This issue will be solved soonish, see [@the1stein's tweet](https://twitter.com/the1stein/status/723550346589659136).

Kubectl command completion in action, it first completes the available k8s resources, and then the available deployments:

{% img /images/k8s_completion.gif 600 kubectl completion %}