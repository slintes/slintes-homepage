---
title: Red Hat
date: 2018-05-01 13:17:19
tags:
    - Golang
    - Kubernetes
---
After some very interesting years at Luminis Technologies, where I enjoyed working with great colleagues on a research project and on a PaaS based on Docker and Kubernetes, I wanted to work more with Go and less with especially OSGi ;) I'm happy to share that today is my first day at Red Hat, where I will work on the [kubevirt](https://www.kubevirt.io/) project. And even better: I'll start my new job in Kopenhagen at the KubeCon conference. Lift-off in a few hours :)