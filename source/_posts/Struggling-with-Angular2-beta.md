---
title: Struggling with Angular2 beta
date: 2016-03-02 10:19:47
tags: Angular2
---
For our CloudRTI project I'm currently busy with a custom frontend for deploying apps to our Kubernetes based environment.
We decided to use Angular2 for it, even though it is in beta yet. I didn't work with Angular1 before, so I started with
a fresh mind.

After going through the [Angular2 tutorial](https://angular.io/docs/ts/latest/tutorial/), I started with our frontend and had some
first nice results quickly. I really like the concepts. You have to get used to the somewhat strange syntax (e.g. `*ngFor` in html),
but that's always a "problem" when using new languages / frameworks.

But the more features I wanted to implement, the more I had to realize that I'm really working with a beta version. Some issues I came across:

* The value of a html select option can only be string. While this is obvious from a pure HTML point of view, I expected that I can use objects in Angular, and I'm not the only one. I was glad to see that the Angular2 team accepted this as a feature request, but until then you have to use a workaround. See https://github.com/angular/angular/issues/4843.

* At some places we enable a feature with a checkbox. When the feature is enabled, a text input field becomes required. So I used Angular to set the readonly and required attribute of the text input based on the checkbox value:
```
<input type="checkbox" [(ngModel)]="checked">
<input type="text" [required]="checked" [readonly]="!checked">
```
    Unfortunately Angular2 does not handle the dynamically added and removed required attribute, and marks the text input and with that
the complete form always as invalid when empty. See https://github.com/angular/angular/issues/5527 and https://github.com/angular/angular/issues/5976.

    As a workaround I implemented a custom Validator, which takes both the checkbox and the text input as input:
```
public static checkboxInputValidator(checkboxName: string, inputName: string, errorCode: string) {
    return (controlGroup: ControlGroup): {[key: string]: any} => {
        let checkbox = controlGroup.controls[checkboxName].value;
        let input = controlGroup.controls[inputName].value;
        if (checkbox && !input) {
            this.addError(inputName, controlGroup.controls[inputName], errorCode);
        }
        else {
            this.removeError(inputName, controlGroup.controls[inputName], errorCode);
        }
        return null;
    }
}
```
    Good thing about this workaround: I had to read about custom validators, which I needed later on anyway :)

* change listeners are not called on selects in Firefox and IE... https://github.com/angular/angular/issues/6573


After a while I wanted to upgrade Angular2 beta2 to beta7... but that was becoming an adventure on itself... nothing compiled anymore after just increasing the version number. Some dependencies had to updated too, I had to add some typescript type definitions which were not needed before, and some 3rd party modules did not work anymore...:

* For the type definitions I use the typings module now... too bad that the type definition for require.js has a bug :/ There is a simple fix available for weeks, but not merged yet: https://github.com/DefinitelyTyped/DefinitelyTyped/pull/7049. So added a fixed version as a custom type definition to our project.

* For authentication and authorization with JWT I use the angular2-jwt module. It had a dependency on another rxjs version than the updated Angular2 beta, and the Observable type changed between these 2 rxjs version, so I got "A is not assignable to B" errors. I could fix the issue and the PR is merged in the meanwhile: https://github.com/auth0/angular2-jwt/issues/39.

* Another module for creating UUIDs failed on some browsers because function definitions are only allowed on top level in strict mode. Not sure if this is new after upgrading Angular, or if I just did not test on other browsers before... however, I could fix this one and the PR is merged now, too: https://github.com/wulfsolter/angular2-uuid/pull/1

All in all this slowed me really down the last weeks. On the other hand digging into such issues give you a better understanding for the tools you use. And contributing back to these tools after you found and fixed issues is always a good thing :)