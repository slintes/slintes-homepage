FROM node:9.4-alpine
COPY . /source
WORKDIR /source
RUN yarn global add hexo-cli
RUN yarn
RUN hexo generate

FROM nginx:stable-alpine
COPY --from=0 /source/public/ /usr/share/nginx/html